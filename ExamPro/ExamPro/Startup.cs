﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExamPro.Startup))]
namespace ExamPro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
