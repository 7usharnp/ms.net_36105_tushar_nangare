﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExamPro.Models
{
    
    public partial class EmployeeDetail
    {
        public class EmployeeMetaData
        {
            [StringLength(10, ErrorMessage = "Name must not be more than 10 char")]
            public string EmpName { get; set; }
            
            [StringLength(6, ErrorMessage = "Password length must be grater than 6 and less than 10.", MinimumLength = 6)]
            public string password { get; set; }

            [Required(ErrorMessage = "Email is required")]
            [StringLength(16, ErrorMessage = "Must be between 5 and 50 characters", MinimumLength = 5)]
            [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
            public string Email { get; set; }
        }
    }
}