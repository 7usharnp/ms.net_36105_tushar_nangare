//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExamPro
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeDetail
    {
        public int Emp_id { get; set; }
        public string EmpName { get; set; }
        public string Designation { get; set; }
        public Nullable<int> Dept_id { get; set; }
        public string Address { get; set; }
        public Nullable<int> City_id { get; set; }
        public Nullable<int> State_id { get; set; }
        public Nullable<int> Country_id { get; set; }
        public string Email_id { get; set; }
        public string Contact_no { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    
        public virtual CityDetail CityDetail { get; set; }
        public virtual Country Country { get; set; }
        public virtual Department Department { get; set; }
        public virtual EmployeeDetail EmployeeDetails1 { get; set; }
        public virtual EmployeeDetail EmployeeDetail1 { get; set; }
        public virtual State State { get; set; }
    }
}
