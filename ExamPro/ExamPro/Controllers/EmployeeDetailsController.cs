﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamPro;

namespace ExamPro.Controllers
{
    public class EmployeeDetailsController : Controller
    {
        private EmployeeManagementEntities db = new EmployeeManagementEntities();

        // GET: EmployeeDetails

        public ActionResult Index()
        {
            var employeeDetails = db.EmployeeDetails.Include(e => e.CityDetail).Include(e => e.Country).Include(e => e.Department).Include(e => e.EmployeeDetails1).Include(e => e.EmployeeDetail1).Include(e => e.State);
            return View(employeeDetails.ToList());
        }

        // GET: EmployeeDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeDetail employeeDetail = db.EmployeeDetails.Find(id);
            if (employeeDetail == null)
            {
                return HttpNotFound();
            }
            return View(employeeDetail);
        }

        
        public string stu_name { get; set; }
        public ActionResult Create()
        {
            ViewBag.City_id = new SelectList(db.CityDetails, "City_id", "CityName");
            ViewBag.Country_id = new SelectList(db.Countries, "Country_id", "CountryName");
            ViewBag.Dept_id = new SelectList(db.Departments, "Dept_id", "DeptName");
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName");
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName");
            ViewBag.State_id = new SelectList(db.States, "State_id", "StateName");
            return View();
        }

        // POST: EmployeeDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Emp_id,EmpName,Designation,Dept_id,Address,City_id,State_id,Country_id,Email_id,Contact_no,username,password")] EmployeeDetail employeeDetail)
        {
            if (ModelState.IsValid)
            {
                db.EmployeeDetails.Add(employeeDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.City_id = new SelectList(db.CityDetails, "City_id", "CityName", employeeDetail.City_id);
            ViewBag.Country_id = new SelectList(db.Countries, "Country_id", "CountryName", employeeDetail.Country_id);
            ViewBag.Dept_id = new SelectList(db.Departments, "Dept_id", "DeptName", employeeDetail.Dept_id);
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName", employeeDetail.Emp_id);
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName", employeeDetail.Emp_id);
            ViewBag.State_id = new SelectList(db.States, "State_id", "StateName", employeeDetail.State_id);
            return View(employeeDetail);
        }

        // GET: EmployeeDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeDetail employeeDetail = db.EmployeeDetails.Find(id);
            if (employeeDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.City_id = new SelectList(db.CityDetails, "City_id", "CityName", employeeDetail.City_id);
            ViewBag.Country_id = new SelectList(db.Countries, "Country_id", "CountryName", employeeDetail.Country_id);
            ViewBag.Dept_id = new SelectList(db.Departments, "Dept_id", "DeptName", employeeDetail.Dept_id);
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName", employeeDetail.Emp_id);
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName", employeeDetail.Emp_id);
            ViewBag.State_id = new SelectList(db.States, "State_id", "StateName", employeeDetail.State_id);
            return View(employeeDetail);
        }

        // POST: EmployeeDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Emp_id,EmpName,Designation,Dept_id,Address,City_id,State_id,Country_id,Email_id,Contact_no,username,password")] EmployeeDetail employeeDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employeeDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.City_id = new SelectList(db.CityDetails, "City_id", "CityName", employeeDetail.City_id);
            ViewBag.Country_id = new SelectList(db.Countries, "Country_id", "CountryName", employeeDetail.Country_id);
            ViewBag.Dept_id = new SelectList(db.Departments, "Dept_id", "DeptName", employeeDetail.Dept_id);
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName", employeeDetail.Emp_id);
            ViewBag.Emp_id = new SelectList(db.EmployeeDetails, "Emp_id", "EmpName", employeeDetail.Emp_id);
            ViewBag.State_id = new SelectList(db.States, "State_id", "StateName", employeeDetail.State_id);
            return View(employeeDetail);
        }

        // GET: EmployeeDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmployeeDetail employeeDetail = db.EmployeeDetails.Find(id);
            if (employeeDetail == null)
            {
                return HttpNotFound();
            }
            return View(employeeDetail);
        }

        // POST: EmployeeDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmployeeDetail employeeDetail = db.EmployeeDetails.Find(id);
            db.EmployeeDetails.Remove(employeeDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
