﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamPro;

namespace ExamPro.Controllers
{
    public class CityDetailsController : Controller
    {
        private EmployeeManagementEntities db = new EmployeeManagementEntities();

        // GET: CityDetails
        public ActionResult Index()
        {
            return View(db.CityDetails.ToList());
        }

        // GET: CityDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityDetail cityDetail = db.CityDetails.Find(id);
            if (cityDetail == null)
            {
                return HttpNotFound();
            }
            return View(cityDetail);
        }

        // GET: CityDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CityDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "City_id,CityName,Status_id")] CityDetail cityDetail)
        {
            if (ModelState.IsValid)
            {
                db.CityDetails.Add(cityDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cityDetail);
        }

        // GET: CityDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityDetail cityDetail = db.CityDetails.Find(id);
            if (cityDetail == null)
            {
                return HttpNotFound();
            }
            return View(cityDetail);
        }

        // POST: CityDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "City_id,CityName,Status_id")] CityDetail cityDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cityDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cityDetail);
        }

        // GET: CityDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityDetail cityDetail = db.CityDetails.Find(id);
            if (cityDetail == null)
            {
                return HttpNotFound();
            }
            return View(cityDetail);
        }

        // POST: CityDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CityDetail cityDetail = db.CityDetails.Find(id);
            db.CityDetails.Remove(cityDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
